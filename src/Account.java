public abstract class Account {
    private double balance;
    private String name;
    private static double interestRate;

    public Account(String val1, double val2){
        name = val1;
        balance = val2;
    }

    public Account(){
        this("Mark", 50.00);
    }

    public void setBalance(double val){
        balance = val;
    }

    public double getBalance(){
        return balance;
    }

    public void setName(String val){
        name = val;
    }

    public String getName(){
        return name;
    }

    public abstract void addInterest();

    public static void setInterestRate(double val){
        interestRate = val;
    }

    public static double getInterestRate(){
        return interestRate;
    }

    public boolean withdraw(double amount){
        if(balance >= amount){
            balance -= amount;
            System.out.println("Money has been withdrawn");
            return true;
        }
        System.out.println("Money has NOT been withdrawn");
       return false;
    }

    public boolean withdraw(){
        if(balance >= 20){
            balance -= 20;
            System.out.println("Money has been withdrawn");
            return true;
        }
        System.out.println("Money has NOT been withdrawn");
        return false;
    }
}
