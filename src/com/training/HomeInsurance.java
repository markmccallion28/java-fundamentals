package com.training;

public class HomeInsurance {
    private double premium;
    private double excess;
    private double amountInsured;

    public double getPremium() {
        return premium;
    }

    public void setPremium(double premium) {
        this.premium = premium;
    }

    public double getAmountInsured() {
        return amountInsured;
    }

    public void setAmountInsured(double amountInsured) {
        this.amountInsured = amountInsured;
    }

    public double getExcess() {
        return excess;
    }

    public void setExcess(double excess) {
        this.excess = excess;
    }
}
