package com.training;

import com.training.person.Person;
import com.training.pets.Dragon;

public class InterfaceMain {

    public static void main(String[] args) {
        Feedable[] thingsWeCanFeed = new Feedable[5];

        Person horace = new Person();

        horace.setName("Horace O Brien");

        Dragon smaug = new Dragon("Smaug");
        smaug.breatheFire();

        thingsWeCanFeed[0] = horace;
        thingsWeCanFeed[1] = smaug;

        for (int i = 0; i < thingsWeCanFeed.length; i++) {
            if (thingsWeCanFeed[i] != null){
                thingsWeCanFeed[i].feed();
            }
        }
    }
}
