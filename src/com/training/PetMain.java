package com.training;

import com.training.pets.Dragon;
import com.training.pets.Pet;

public class PetMain {
    public static void main(String[] args) {
        Pet myPet = new Pet();
        myPet.feed();

        Dragon myDragon = new Dragon();
        myDragon.setNumLegs(4);
        myDragon.getNumLegs();
    }
}

