public class TestInheritance {
    public static void main(String[] args) {
        Account[] accounts = new Account[3];
        accounts[0] = new SavingsAccount("Mark", 2);
        accounts[1] = new SavingsAccount("John", 4);
        accounts[2] = new CurrentAccount("Matthew", 6);

        for( Account x : accounts){
            System.out.println(x.getName() + " " +  x.getBalance());
            x.addInterest();
            System.out.println(x.getName() + " " + x.getBalance());
        }
    }
}
