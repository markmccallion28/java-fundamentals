public class SavingsAccount extends Account {

    public SavingsAccount(String name, double balance){
        super(name, balance);
    }

    @Override
    public void addInterest(){
        super.setBalance(super.getBalance() * 1.4);
    }
}
