public class CurrentAccount extends Account{

    public CurrentAccount(String name, double balance){
        super(name, balance);
    }

    @Override
    public void addInterest(){
        super.setBalance(super.getBalance() * 1.1);
    }
}
