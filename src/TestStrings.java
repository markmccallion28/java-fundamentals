import java.util.Date;

public class TestStrings {

    public static void main(String[] args) {
        String x = "example.doc";
        x = "example.bak";

        String s1 = "test";
        String s2 = "test";

        if(s1 == s2){
            System.out.println("x is the same object as y");
        }

        if(s1.equals(s2)){
            System.out.println("x has the same contents as y");
        }

        String sentence = "the quick brown fox swallowed down the lazy chicken";
        int ans = sentence.split("ow", -1).length - 1;
        System.out.println(ans);


        String sentence2 = "Live not on evil";
        int start = 0;
        int end = sentence2.length()-1;
        boolean found = true;

        while(start < end){
            if (sentence2.charAt(start) != sentence2.charAt(end)){
                found = false;
                break;
            }
            start++;
            end--;
        }
        System.out.println(found);

        Date d1 = new Date();
        System.out.println("Current date is " + d1);
    }
}
